FROM alpine

RUN apk add --no-cache --virtual .run-deps rsync && rm -rf /var/cache/apk/*
